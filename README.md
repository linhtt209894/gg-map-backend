# Google Map Backend

Node.js Express, Sequelize & PostgresSQL

---

## Requirements

For development, you will only need Node.js and a node global package, Postgres.

### Node, npm

Set up a Node.js Development Environment and npm with the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

If the installation was successful, you should be able to run:

    $ node --version
    v16.2.0

    $ npm --version
    7.13.0

### Postgres

Install Postgres on the [Posgres setup guide](https://computingforgeeks.com/how-to-install-postgis-on-ubuntu-debian/) and create 'testdbpg' database with the [following commands]() or create in pgAdmin web:

       $ sudo -i -u postgres
       $ psql
       $ postgres=# CREATE DATABASE testdbpg;
       $ postgres=# \c testdbpg
       $ postgres=# CREATE EXTENSION Postgis;

## Install

    $ git clone https://github.com/YOUR_USERNAME/PROJECT_TITLE
    $ cd PROJECT_TITLE
    $ cd server
    $ npm i

## Configure app

1. Copy file `env.example` to `.env`

2. Edit `.env` file variable following by your environment.<br/>
   For example: <br/>
   `DB_DIALECT=4000`<br/>
   `DB_HOST=localhost`<br/>
   `DB_USER=postgres`<br/>
   `DB_PASS=postgres`<br/>
   `DB_NAME=testdbpg`<br/>
   `DB_DIALECT=postgres`<br/>
   `DB_PORT=5432`<br/>
   `APP_HOST=localhost`

## Running the project

    $ npm run dev

## Request & Response Examples

### API Resources

- [POST /place/add](#post-place)
- [POST /place/addM](#post-multiple-place)
- [POST /place/detail](#post-detail-place)
- [GET /place/list](#get-all-submissions)
- [GET /place/list/pending](#get-all-pending-submissin)
- [POST /place/list/radius](#post-all-arrount-approved-submissions)
- [POST /place/update](#post-place-update)

### POST /place/add

Example: http://localhost/api/place/add

Request body:

    {
        "address": "Ha Noi",
        "type": 1,
        "name": "Ho Guom",
        "location": [21.068511, 105.804817]
    }

### POST /place/addM

Example: http://localhost/api/place/addM

Request body:

    {
        "places" : [
            {
                "address": "Ha Noi",
                "type": 1,
                "name": "Ho Guom",
                "location": [21.068511, 105.804817]
            },
            {
                "address": "Nghe An",
                "type": 3,
                "name": "Cho Vinh",
                "location": [11.068511, 95.804817]
            },
        ]

    }

### POST /place/detail

Example: http://localhost/api/place/detail

Request body:

    {
        "id": 1,
    }

Response body:

       {
        "id": 1,
        "name": "adsfasdffad",
        "location": {
            "crs": {
                "type": "name",
                "properties": {
                }
            },
            "type": "Point",
            "coordinates": [
                21.068511,
                105.804817
            ]
        },
        "address": "Liddndsdfah",
        "type": 1,
        "status": 3,
        "createdAt": "2021-07-26T18:32:15.892Z",
        "updatedAt": "2021-07-26T18:32:15.892Z"
    }

### GET /place/list

Example: http://localhost/api/place/list

Response body:

    {
        "places": [
            {
                "id": 1,
                "name": "adsfasdffad",
                "location": {
                    "crs": {
                        "type": "name",
                        "properties": {
                        }
                    },
                    "type": "Point",
                    "coordinates": [
                        21.068511,
                        105.804817
                    ]
                },
                "address": "Liddndsdfah",
                "type": 1,
                "status": 3,
                "createdAt": "2021-07-26T18:32:15.892Z",
                "updatedAt": "2021-07-26T18:32:15.892Z"
            }
        ]
    }

### GET /place/list/pending

Example: http://localhost/api/place/list/pending

Response body:

    {
        "places": [
            {
                "id": 1,
                "name": "adsfasdffad",
                "location": {
                    "crs": {
                        "type": "name",
                        "properties": {
                        }
                    },
                    "type": "Point",
                    "coordinates": [
                        21.068511,
                        105.804817
                    ]
                },
                "address": "Liddndsdfah",
                "type": 1,
                "status": 3,
                "createdAt": "2021-07-26T18:32:15.892Z",
                "updatedAt": "2021-07-26T18:32:15.892Z"
            }
        ]
    }

### GET /place/list/raidus

Example: http://localhost/api/place/list/radius

Request body:

    {
        "location": [
                21.068511,
                105.804817
            ]

    }

### POST /place/update

Example: http://localhost/api/place/update

Request body:

    {
        "id": "1",
        "status": 1,
        "location": [21.068511, 105.804817]
    }
