export default {
    MODE: "PRODUCTION",
    HASH_ALGORITHMS: "SHA256",
    RADIUS: 20,

    PLACE_STATUS: {
        APPROVE: 1,
        DENY: 2,
        PENDING: 3
    },

    PLACE_TYPE: {
        HOSPITAL: 1,
        SCHOOL: 2,
        PARK: 3,
        SUPERMARKET: 4
    },

    API: {
        PREFIX: {
            PLACE: "/place"
        },
        PLACE_API: {
            PLACE_ADD: "/add",
            PLACE_ADD_MULTIPLE: "/addM",
            PLACE_DETAIL: "/detail",
            PLACE_UPDATE: "/update",
            PLACE_LIST: "/list",
            PLACE_LIST_APPROVE_20KM: "/list/radius",
            PLACE_LIST_PENDING: "/list/pending",
            
        }
    }

}