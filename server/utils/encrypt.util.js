import crypto from "crypto";
import Directory from "./directory.util.js";

function encryptPassword(password, salt, algo) {
    const hash = crypto.pbkdf2Sync(password, salt, 100000, 64, algo).toString('hex');
    return hash;
}

function generatePassword(password) {
    const salt = crypto.randomBytes(16).toString('hex');
    const hash = encryptPassword(password, salt, Directory.HASH_ALGORITHMS);
    console.log(password);
    console.log(hash);
    console.log(salt);
    return {
        salt: salt,
        hash: hash,
        algo: Directory.HASH_ALGORITHMS
    }
}

export {
    encryptPassword, generatePassword
}
