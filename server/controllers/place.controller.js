import HttpStatus from "http-status-codes";

// Model 
import * as PlaceModel from "../models/place.model.js";

// Util
import * as CryptoUtil from "../utils/encrypt.util.js";
import DirectoryUtil from "../utils/directory.util.js";


function checkRequiredFields(fileds, res) {
    const isMissing = fileds.some(field => !field);
    if(isMissing) {
        return res.status(HttpStatus.NOT_ACCEPTABLE).send({ message: "You must provide required fields!" });
    }
}

async function createOne(req, res) {
    const name = req.body.name;
    const point = req.body.location;
    const address = req.body.address;
    const type = req.body.type;

    // Check required fields
    checkRequiredFields([name, point, address, type], res);

    // Check type
    const typePattern = /[1-4]{1}/;
    if(!typePattern.test(type))
        return res.status(HttpStatus.NOT_ACCEPTABLE).send({ message: "Place's type is invalid!" });

    try {        
        // Create place
        let placecheck = await PlaceModel.create(name, point, address, type, DirectoryUtil.PLACE_STATUS.PENDING);

        if(!placecheck)
            return res.status(HttpStatus.METHOD_FAILURE).send({ message: 'Cannot create place, please try later' });       
        
        return res.status(HttpStatus.OK).send({ message: 'Create successfully!' });       
    } catch (error) {
        return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ message: error, data: null });
    }
};

async function createMultiple(req, res) {
    const places = req.body.places;    
    try {        
        for (let place in places) {
            // Check required fields
            checkRequiredFields([place.name, place.point, place.address, place.type], res);
    
            // Check type
            const typePattern = /[1-4]{1}/;
            if(!typePattern.test(place.type))
                return res.status(HttpStatus.NOT_ACCEPTABLE).send({ message: "Input is invalid!" });
            
            const currentPlace = {
                name: place.name,
                location: place.location,
                address: place.address,
                type: place.type,
                status: DirectoryUtil.PLACE_STATUS.PENDING
            }
            let placecheck = await PlaceModel.create(currentPlace);
        }        
        return res.status(HttpStatus.OK).send({ message: 'Create successfuly' });       
    } catch (error) {
        return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ message: error, data: null });
    }
};

async function getPlace(req, res) {
    const id = req.body.id;

    // Check require field
    checkRequiredFields([id], res);

    // Get detail place
    const place = await PlaceModel.getPlace('id', id);
    if(!place) 
        return res.status(HttpStatus.NOT_FOUND).send({ message: "Not found!" });
    return res.status(HttpStatus.OK).send({ place: place });
}

async function getPendingSubmissions(req, res) {
    try {
        // Get all pending submissions
        const pendingPlaces = await PlaceModel.getAllPlacesWith('status', DirectoryUtil.PLACE_STATUS.PENDING);
        return res.status(HttpStatus.OK).send({ places: pendingPlaces }); 
    } catch (error) {
        return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ message: error, data: null });
    }
}


async function getAllPlaces(req, res) {
    try {
        // Get all pending submissions
        const places = await PlaceModel.getAllPlaces();
        return res.status(HttpStatus.OK).send({ places: places }); 
    } catch (error) {
        return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ message: error, data: null });
    }
}

async function getPlacesWithinRadius(req, res) {
    try {
        const geo = req.body.location;

        // Check require field
        checkRequiredFields([geo], res);

        // Get all pending submissions
        const places = await PlaceModel.getAllPlacesWithinRadius(geo, DirectoryUtil.RADIUS);
        return res.status(HttpStatus.OK).send({ places: places }); 
    } catch (error) {
        return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ message: error, data: null });
    }
}

async function updateSubmission(req, res) {
    try {
        const id = req.body.id;
        const status = req.body.status;
        const point = req.body.location;
    
        // Check required fields
        checkRequiredFields([id, status, point], res);

        // Check is exist place?
        const place = PlaceModel.getPlace('id', id);
        if(!place) {
            return res.status(HttpStatus.NOT_FOUND).send({ message: "This submission is nonexistent" });
        }
        
        // Check status
        if(status !== DirectoryUtil.PLACE_STATUS.APPROVE && status !== DirectoryUtil.PLACE_STATUS.DENY) {
            return res.status(HttpStatus.NOT_ACCEPTABLE).send({ message: "This submission's status is invalid" });
        }

        // Update old approved submission 
        let currentApprovedPlace = updateOldApprovedPlace(res, point, DirectoryUtil.PLACE_STATUS.DENY);
        
        // Update submission
        const updatePlace = await PlaceModel.updatePlace(id, status);
        if(!updatePlace) {
            // rollback old approved submission 
            await PlaceModel.updatePlace(currentApprovedPlace.id, DirectoryUtil.PLACE_STATUS.APPROVE);
        }
        return res.status(HttpStatus.OK).send({ message: 'Update successfully', place: updatePlace }); 
    } catch (error) {
        return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ message: error, data: null });
    }
}

// async function updateSubmissio1n(req, res) {
//     try {
//         const point = req.body.location;
    
//         const updatePlace = await PlaceModel.getCurrentApprovedPlace(point);

//         return res.status(HttpStatus.OK).send({ message: 'sdf', place: updatePlace }); 
//     } catch (error) {
//         return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ message: error, data: null });
//     }
// }

async function updateOldApprovedPlace(res, point, status) {
    try {
        // Get current approved place 
        let currentApprovedPlace = await PlaceModel.getCurrentApprovedPlace(point, DirectoryUtil.PLACE_STATUS.APPROVE);

        // Update status
        if(currentApprovedPlace) {
            currentApprovedPlace = await PlaceModel.updatePlace(currentApprovedPlace.id, status);
        }
        return currentApprovedPlace;
    } catch (error) {
        return res.status(HttpStatus.SERVICE_UNAVAILABLE).send({ message: error, data: null });
    }
}


export {
    createOne, getPlace, getPendingSubmissions, updateSubmission, 
    getPlacesWithinRadius, createMultiple, getAllPlaces
};