import db from "../entities/index.js"
import Directory from "../utils/directory.util.js";
import sequelize from "sequelize";
const Place = db.Place;
const {Op} = sequelize;


async function create(name, location, address, type, status) {
    try {
        const newPlace = {
            name: name,
            location: {
                type: 'Point',
                coordinates: location
            },
            address: address,
            type: type,
            status: status
        };
        await Place.create(newPlace);
        return newPlace;
    } catch (error) {
        console.log(error);
        return null;
    }
}
async function createPlaces(places) {
    try {
        return await Place.bulkCreate(places);
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function getPlace(key, value) {
    try {
        return await Place.findOne({
            where: { [`${key}`]: value }
        });
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function getCurrentApprovedPlace(point) {
    try {
        // const distanceHost = 6371 * acos(cos(radians(point[0])) * cos(radians(ST_X(location))) * cos(radians(point[1]) - radians(ST_Y(location))) + sin(radians(point[1])) * sin(radians(ST_X(location))));
        const distance = sequelize.literal(
            "6371 * acos(cos(radians("+geo[0]+")) * cos(radians(ST_X(location))) * cos(radians("+geo[1]+") - radians(ST_Y(location))) + sin(radians("+geo[0]+")) * sin(radians(ST_X(location))))"
            );
        return await Place.findOne({
            where: [
                // sequelize.where(distance, { [Op.eq]: distanceHost }),
                { status: Directory.PLACE_STATUS.APPROVE}
            ]
        });
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function getAllPlacesWith(key, value) {
    try {
        return await Place.findAll({
            where: { [`${key}`]: value }
        });
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function getAllPlaces() {
    try {
        return await Place.findAll();
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function getAllPlacesWithinRadius(geo, radius) {
    try {
        // Calculate distance
        const distance = sequelize.literal(
            "6371 * acos(cos(radians("+geo[0]+")) * cos(radians(ST_X(location))) * cos(radians("+geo[1]+") - radians(ST_Y(location))) + sin(radians("+geo[0]+")) * sin(radians(ST_X(location))))"
            );
        return await Place.findAll({
            where: [
                sequelize.where(distance, { [Op.lte]: radius }),
                { status: Directory.PLACE_STATUS.APPROVE}
            ]
        })
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function getPendingSubmissions() {
    try {
        return await Place.findAll({
            where: { status: Directory.PLACE_STATUS.PENDING }
        });
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function updatePlace(id, status) {
    try {
        const updatePlace = await Place.update(
            { status:  status},
            { where: { id: id } }
        )
        return updatePlace;
    } catch (error) {
        console.log(error);
        return null;
    }
}


export { 
    create, getPlace, updatePlace, getPendingSubmissions, getAllPlaces, 
    getCurrentApprovedPlace, getAllPlacesWithinRadius, createPlaces, getAllPlacesWith
};
