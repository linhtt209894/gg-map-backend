import express from 'express';
import cors from "cors";
import swaggerUi from "swagger-ui-express";
import 'dotenv/config';
import db from "./entities/index.js";

const app = express();

// App config
var corsOptions = {
    origin: "http://localhost:4001"
};
app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

db.sequelize.sync();
// // drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });

// Swagger setup
import swaggerDocs from "./config/swagger.config.js"
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// Routes
import placeRoutes from "./routes/place.route.js";
app.use("/api/place", placeRoutes);
  
// Start the server
const port = process.env.APP_PORT || 4000;
app.listen(port, () => {
    console.log(`Express server listening on port ${port}.`);
});