import express from 'express';
const router = express.Router();

// Controller
import * as PlaceController from "../controllers/place.controller.js";

// Directory
import Directory from "../utils/directory.util.js";

// Public routes
router.post(Directory.API.PLACE_API.PLACE_ADD, async function (req, res) {
    await PlaceController.createOne(req, res);
});

router.post(Directory.API.PLACE_API.PLACE_DETAIL, async function (req, res) {
    await PlaceController.getPlace(req, res);
});
/**
 * @swagger
 * /place/detail:
 *   post:
 *      description: Get detail place by place id
 *      tags:
 *          - place
 *      parameters:
 *          - in: body
 *            name: Post
 *            description: Post data
 *            schema:
 *              type: object
 *              required:
 *                 - id
 *              properties:
 *                  id:
 *                      type: integer
 *                      example: 1
 *      responses:
 *          '200':
 *              description: OK Return detail place
 *          '500':
 *              description: Internal server error
 *          '400':
 *              description: Not found
 */
router.get(Directory.API.PLACE_API.PLACE_LIST_PENDING, async function (req, res) {
    await PlaceController.getPendingSubmissions(req, res);
});
/**
 * @swagger
 * /place/list/pending:
 *   get:
 *      description: Get all pending submissions
 *      tags:
 *          - place
 *      parameters:
 *          - in: body
 *            name: Get
 *            description: Get data
 *      responses:
 *          '200':
 *              description: OK Return all pending submissions
 *          '500':
 *              description: Internal server error
 */

router.get(Directory.API.PLACE_API.PLACE_LIST, async function (req, res) {
    await PlaceController.getAllPlaces(req, res);
});
/**
 * @swagger
 * /place/list/:
 *   get:
 *      description: Get all submissions
 *      tags:
 *          - place
 *      parameters:
 *            name: Get
 *            description: Get data
 *      responses:
 *          '200':
 *              description: OK Return all submissions
 *          '500':
 *              description: Internal server error
 */

router.get(Directory.API.PLACE_API.PLACE_LIST_APPROVE_20KM, async function (req, res) {
    await PlaceController.getPlacesWithinRadius(req, res);
});
/**
 * @swagger
 * /place/list/radius:
 *   post:
 *      description: Get all submissions from marker point to 20km radius
 *      tags:
 *          - place
 *      parameters:
 *          - in: body
 *            name: Post
 *            description: Post data
 *            schema:
 *              type: object
 *              required:
 *                 - location
 *              properties:
 *                  location:
 *                      type: array
 *                      example: [20.960999755129, 105.59018564258]
 *      responses:
 *          '200':
 *              description: OK Return detail place
 *          '500':
 *              description: Internal server error
 *          '400':
 *              description: Not found
 */

router.post(Directory.API.PLACE_API.PLACE_UPDATE, async function (req, res) {
    await PlaceController.updateSubmission(req, res);
});

router.post(Directory.API.PLACE_API.PLACE_ADD_MULTIPLE, async function (req, res) {
    await PlaceController.createMultiple(req, res);
});


export default router;