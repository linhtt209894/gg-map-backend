export default (sequelize, DataTypes) => {
    const Place = sequelize.define("place", {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull:  false,
            primaryKey: true,
        },
        location: {
            type: DataTypes.GEOMETRY,
            allowNull:  false,
            primaryKey: true,
        },
        address: {
            type: DataTypes.STRING,
            allowNull:  false,
            primaryKey: true,
        },
        type: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
        }
    },
    {
        freezeTableName: true
    });
  
    return Place;
};