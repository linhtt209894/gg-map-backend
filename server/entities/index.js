import sequelize from "../config/sequelize.config.js";
import DataTypes from "sequelize";
import Place from "./place.entity.js";

const db = {};

db.Sequelize = DataTypes;
db.sequelize = sequelize;

db.Place = Place(sequelize, DataTypes);

export default db;