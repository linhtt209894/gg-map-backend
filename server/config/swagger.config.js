import swaggerJSDoc from "swagger-jsdoc";

// Swagger setup
const swaggerOption = {
    swaggerDefinition: (swaggerJSDoc.Options = {
        info: {
            title: "GG Map",
            description: "API documentation",
            contact: {
                name: "Developer",
            },
            version: "1.0.0"
        },
    }),
    apis: ["./routes/*.js"],
};

const swaggerDocs = swaggerJSDoc(swaggerOption);

export default swaggerDocs;